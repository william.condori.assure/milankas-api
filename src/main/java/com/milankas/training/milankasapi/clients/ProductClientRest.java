package com.milankas.training.milankasapi.clients;

import com.milankas.training.milankasapi.domain.dto.product.ProductResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "microservice-product")
@RequestMapping("/v1/products")
public interface ProductClientRest {

    @GetMapping("/{productId}")
    ResponseEntity<ProductResponseDTO> getProduct(@PathVariable("productId") String productId);

    @GetMapping("/")
    ResponseEntity<List<ProductResponseDTO>> getAll(@RequestParam(value = "companyId", required = false) String companyId);

}
