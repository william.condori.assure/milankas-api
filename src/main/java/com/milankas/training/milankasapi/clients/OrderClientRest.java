package com.milankas.training.milankasapi.clients;

import com.milankas.training.milankasapi.domain.dto.order.OrderResponseDTO;
import com.milankas.training.milankasapi.validation.ValidUUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "microservice-order")
@RequestMapping("/v1/orders")
public interface OrderClientRest {

    @GetMapping("/{orderId}")
    ResponseEntity<OrderResponseDTO> getOrder(@ValidUUID @PathVariable("orderId") String orderId);

}
