package com.milankas.training.milankasapi.domain.dto.order;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class OrderResponseDTO {

    private UUID id;
    private String customerName;
    private String emailAddress;
    private AddressResponseDTO shippingAddress;
    private List<LineItemResponseDTO> lineItems;

}
