package com.milankas.training.milankasapi.domain.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.milankas.training.milankasapi.domain.dto.product.ProductResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class LineItemResponseDTO {

    private UUID id;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private UUID productId;
    private Integer qty;
    private ProductResponseDTO product;

}
