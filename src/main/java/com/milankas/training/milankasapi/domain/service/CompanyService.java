package com.milankas.training.milankasapi.domain.service;

import com.milankas.training.milankasapi.clients.CompanyClientRest;
import com.milankas.training.milankasapi.clients.ProductClientRest;
import com.milankas.training.milankasapi.domain.dto.company.CompanyResponseDTO;
import com.milankas.training.milankasapi.web.controller.exception.ApiRequestException;
import com.milankas.training.milankasapi.web.controller.exception.ApiRequestExceptionNotFound;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyClientRest companyClientRest;

    @Autowired
    private ProductClientRest productClientRest;

    public CompanyResponseDTO getCompanyProducts(String companyId) {
        try {
            CompanyResponseDTO companyResponseDTO = companyClientRest.getCompany(companyId).getBody();
            companyResponseDTO.setProducts(productClientRest.getAll(companyId).getBody());
            return companyResponseDTO;
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                throw new ApiRequestExceptionNotFound();
            }
            throw new ApiRequestException("microservice-company is not available");
        }
    }
}
