package com.milankas.training.milankasapi.domain.dto.company;

import com.milankas.training.milankasapi.domain.dto.product.ProductResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CompanyResponseDTO {

    private String id;
    private String name;
    private AddressResponseDTO address;
    private List<ProductResponseDTO> products;

}

