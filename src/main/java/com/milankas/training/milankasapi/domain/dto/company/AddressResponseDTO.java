package com.milankas.training.milankasapi.domain.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressResponseDTO {

    private String addressLine1;
    private String addressLine2;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;

}
