package com.milankas.training.milankasapi.domain.dto.product;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ProductResponseDTO {

    private UUID id;
    private String name;
    private String description;
    private String companyId;
    private Boolean blocked;
    private List<String> categories;

}
