package com.milankas.training.milankasapi.domain.service;

import com.milankas.training.milankasapi.clients.OrderClientRest;
import com.milankas.training.milankasapi.clients.ProductClientRest;
import com.milankas.training.milankasapi.domain.dto.order.OrderResponseDTO;
import com.milankas.training.milankasapi.domain.dto.product.ProductResponseDTO;
import com.milankas.training.milankasapi.web.controller.exception.ApiRequestException;
import com.milankas.training.milankasapi.web.controller.exception.ApiRequestExceptionNotFound;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderClientRest orderClientRest;

    @Autowired
    private ProductClientRest productClientRest;

    public OrderResponseDTO getProductsBoughtInOrder(String orderId) {
        try {

            OrderResponseDTO orderResponseDTO = orderClientRest.getOrder(orderId).getBody();
            orderResponseDTO.getLineItems()
                    .stream()
                    .map(lineItemResponseDTO -> {
                                ProductResponseDTO product = productClientRest.getProduct(lineItemResponseDTO.getProductId().toString()).getBody();
                                lineItemResponseDTO.setProduct(product);
                                return lineItemResponseDTO;
                            }

                    )
                    .collect(Collectors.toList());

            return orderResponseDTO;
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                throw new ApiRequestExceptionNotFound();
            }
            throw new ApiRequestException("microservice-order is not available");
        }
    }
}
