package com.milankas.training.milankasapi.web.controller;

import com.milankas.training.milankasapi.domain.dto.company.CompanyResponseDTO;
import com.milankas.training.milankasapi.domain.service.CompanyService;
import com.milankas.training.milankasapi.validation.ValidUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/companies")
@Validated
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/{companyId}/products")
    public ResponseEntity<CompanyResponseDTO> getCompanyProducts(@PathVariable("companyId") @ValidUUID String companyId) {
        return ResponseEntity.ok(companyService.getCompanyProducts(companyId));
    }
}
