package com.milankas.training.milankasapi.web.controller;

import com.milankas.training.milankasapi.domain.dto.order.OrderResponseDTO;
import com.milankas.training.milankasapi.domain.service.OrderService;
import com.milankas.training.milankasapi.validation.ValidUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/orders")
@Validated
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/{orderId}/products")
    public ResponseEntity<OrderResponseDTO> getProductsBoughtInOrder(@PathVariable("orderId") @ValidUUID String orderId){
        return ResponseEntity.ok(orderService.getProductsBoughtInOrder(orderId));
    }

}
